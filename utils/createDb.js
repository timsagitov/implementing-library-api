const createBookList = require("./createBookList");
const fs = require("fs");

const db = JSON.stringify(createBookList());
fs.writeFile("db.json", db, (err) => {
  if (err) console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("db.json", "utf8"));
  }
});

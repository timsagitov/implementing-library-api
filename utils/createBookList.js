module.exports = () => {
  const bookGenres = [
    "Adventure",
    "Comic",
    "Crime",
    "Drama",
    "Fiction",
    "Fantasy",
    "Historical",
    "Horror",
    "Magic",
    "Realism",
    "Mystery",
    "Philosophy",
    "Politics",
    "Romance",
    "Saga",
    "Satire",
    "Science",
    "Superhero",
    "Thriller",
    "Urban",
    "Western",
  ];

  const books = [
    ["Don Quixote", "Miguel de Cervantes"],
    ["Alice's Adventures in Wonderland", "Lewis Carroll"],
    ["The Adventures of Huckleberry Finn", "Mark Twain"],
    ["The Adventures of Tom Sawyer", "Mark Twain"],
    ["Treasure Island", "Robert Louis Stevenson"],
    ["Pride and Prejudice", "Jane Austen"],
    ["Wuthering Heights", "Emily Brontë"],
    ["Jane Eyre", "Charlotte Brontë"],
    ["Moby Dick", "Herman Melville"],
    ["The Scarlet Letter", "Nathaniel Hawthorne"],
    ["Gulliver's Travels", "Jonathan Swift"],
    ["The Pilgrim's Progress", "John Bunyan"],
    ["A Christmas Carol", "Charles Dickens"],
    ["David Copperfield", "Charles Dickens"],
    ["A Tale of Two Cities", "Charles Dickens"],
    ["Little Women", "Louisa May Alcott"],
    ["Great Expectations", "Charles Dickens"],
    ["The Hobbit, or, There and Back Again", "J.R.R. Tolkien"],
    ["Frankenstein, or, the Modern Prometheus", "Mary Shelley"],
    ["Oliver Twist", "Charles Dickens"],
    ["Uncle Tom's Cabin", "Harriet Beecher Stowe"],
    ["Crime and Punishment", "Fyodor Dostoyevsky"],
    ["Madame Bovary: Patterns of Provincial life", "Gustave Flaubert"],
    ["The Return of the King", "J.R.R. Tolkien"],
    ["Dracula", "Bram Stoker"],
    ["The Three Musketeers", "Alexandre Dumas"],
    ["Brave New World", "Aldous Huxley"],
    ["War and Peace", "Leo Tolstoy"],
    ["To Kill a Mockingbird", "Harper Lee"],
  ];

  const createRandomISBN = () => {
    const eanPrefixes = ["978", "979"];
    let randomEanPrefix = eanPrefixes[Math.floor(Math.random() * 2)].toString();
    let randomRegistrationGroup = Math.floor(
      Math.random() * (99 - 10 + 1) + 10
    ).toString();
    let randomRegistrant = Math.floor(
      Math.random() * (99999 - 10000 + 1) + 10000
    ).toString();
    let randomPublication = Math.floor(
      Math.random() * (99 - 10 + 1) + 10
    ).toString();
    let randomCheckDigit = Math.floor(
      Math.random() * (9 - 1 + 1) + 1
    ).toString();
    return `${randomEanPrefix}-${randomRegistrationGroup}-${randomRegistrant}-${randomPublication}-${randomCheckDigit}`;
  };

  const finalBookList = [];
  books.forEach((book, index) => {
    finalBookList.push({
      id: index,
      name: book[0],
      author: book[1],
      ISBN: createRandomISBN(),
      genre: bookGenres[Math.floor(Math.random() * bookGenres.length)],
      reserved: false,
    });
  });

  // const randomBook = books[Math.floor(Math.random() * books.length)];
  // const randomBookName = randomBook[0].toString();
  // const randomBookAuthor = randomBook[1].toString();
  // const randomBookISBN = createRandomISBN();
  // const randomBookGenre =
  //   bookGenres[Math.floor(Math.random() * bookGenres.length)];

  return finalBookList;
};

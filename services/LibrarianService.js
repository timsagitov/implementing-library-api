const fs = require("fs");
const util = require("util");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

class BookService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the books data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  async getBookByID(id) {
    const data = await this.getData();
    for (let shelf of data) {
      for (let book of shelf) {
        if (book.id === id) return book;
      }
    }
    return "There is no such book in our library";
  }

  /**
   * Add a new book on a shelf
   * @param {*} name The name of the book
   */
  async addBookOnShelf(book, shelfId) {
    const data = (await this.getData()) || [];
    data[shelfId].push(book);
    return writeFile(this.datafile, JSON.stringify(data));
  }

  async changeShelf(bookId, shelfIdFrom, shelfIdTo) {
    const data = (await this.getData()) || [];
    const bookIndex = data[shelfIdFrom].findIndex((book) => book.id === bookId);
    const book = data[shelfIdFrom].splice(bookIndex, 1);
    data[shelfIdTo].push(book[0]);
    return writeFile(this.datafile, JSON.stringify(data));
  }

  async deleteEntry(name) {
    const data = await this.getData();
    const index = data.indexOf({ name });
    data.splice(index, 1);
    return writeFile(this.datafile, JSON.stringify(data));
  }

  /**
   * Fetches book data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    if (!data) return [];
    return JSON.parse(data);
  }
}

module.exports = BookService;

const fs = require("fs");
const util = require("util");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

class ReservationService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the books data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  async getTakenBooks() {
    const data = await this.getData();
    const takenBooks = [];
    for (book of data) {
      if (currentReader) takenBooks.push(book);
    }
    return takenBooks;
  }

  /**
   * Fetches book data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    if (!data) return [];
    return JSON.parse(data);
  }
}

module.exports = ReservationService;

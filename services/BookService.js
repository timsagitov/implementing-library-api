const fs = require("fs");
const util = require("util");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

class BookService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the books data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  /**
   * Get all book items
   */
  async getList() {
    const data = await this.getData();
    return data;
  }

  async getByName(name) {
    const data = await this.getData();
    for (let shelf of data) {
      for (let book of shelf) {
        if (book.name === name) return book;
      }
    }
    return "There is no such book in our library";
  }

  async getByISBN(isbn) {
    const data = await this.getData();
    for (let shelf of data) {
      for (let book of shelf) {
        if (book.ISBN === isbn) return book;
      }
    }
    return "There is no such book in our library";
  }

  async getByGenre(genre) {
    const data = await this.getData();
    const foundBooks = [];
    for (let shelf of data) {
      for (let book of shelf) {
        if (book.genre === genre) foundBooks.push(book);
      }
    }
    return foundBooks;
  }

  async getByAuthor(author) {
    const data = await this.getData();
    const foundBooks = [];
    for (let shelf of data) {
      for (let book of shelf) {
        if (book.author === author) foundBooks.push(book);
      }
    }
    return foundBooks;
  }

  async getShelf(id) {
    const data = await this.getData();
    return data[id];
  }

  /**
   * Add a new book item
   * @param {*} name The name of the book
   */
  async addEntry(name) {
    const data = (await this.getData()) || [];
    data.unshift({ name });
    return writeFile(this.datafile, JSON.stringify(data));
  }

  async deleteEntry(name) {
    const data = await this.getData();
    const index = data.indexOf({ name });
    data.splice(index, 1);
    return writeFile(this.datafile, JSON.stringify(data));
  }

  /**
   * Fetches book data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    if (!data) return [];
    return JSON.parse(data);
  }
}

module.exports = BookService;

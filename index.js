const express = require("express");
const routes = require("./routes");
const BookService = require("./services/BookService");
const LibrarianService = require("./services/LibrarianService");
const ReservationService = require("./services/ReservationService");

const app = express();
const PORT = 3000;
const bookService = new BookService("./data/bookShelves.json");
const librarianService = new LibrarianService("./data/bookShelves.json");
const reservationService = new ReservationService("./data/takenBooks.json");

// For pulling from req.body
app.use(express.json());

app.use("/", routes({ bookService, librarianService, reservationService }));

app.listen(PORT, () => {
  console.log(`Server is listening on PORT - ${PORT}`);
});

const express = require("express");

const router = express.Router();

module.exports = (params) => {
  const { librarianService } = params;
  router.get("/", (req, res) => {
    res.send("Hello from /librarian");
  });

  // (2) A librarian should be able to put a new book on a shelf.
  router.post("/add/:shelfId", async (req, res) => {
    const newBook = req.body;
    await librarianService.addBookOnShelf(newBook, req.params.shelfId);
    res.send(`The book was successfully put on a shelf ${req.params.shelfId}`);
  });

  // (3) A librarian should be able to move a book between shelves.
  router.post("/move/:id/:from/:to", async (req, res) => {
    await librarianService.changeShelf(
      Number(req.params.id),
      req.params.from,
      req.params.to
    );
    res.send(
      `The book was successfully moved from shelf ${req.params.from} to ${req.params.to}`
    );
  });

  // (4) A librarian should be able to get the information on a book using its ID.
  router.get("/book/:id", async (req, res) => {
    const data = await librarianService.getBookByID(Number(req.params.id));
    res.json(data);
  });

  // Reservation business

  // (5) A librarian should know which reader took a book.

  return router;
};

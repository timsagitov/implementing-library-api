const express = require("express");

const router = express.Router();

module.exports = (params) => {
  const { bookService } = params;
  router.get("/", (req, res) => {
    res.send("Hello from /books");
  });

  // (1a) Get book by it's name
  router.get("/name/:name", async (req, res) => {
    const data = await bookService.getByName(req.params.name);
    res.json(data);
  });

  // (1a) Get book by it's ISBN
  router.get("/isbn/:isbn", async (req, res) => {
    const data = await bookService.getByISBN(req.params.isbn);
    res.json(data);
  });

  // (1c-d) Get all books by genre
  router.get("/genre/:genre", async (req, res) => {
    const data = await bookService.getByGenre(req.params.genre);
    res.json(data);
  });

  // (1c-d) Get all books by author
  router.get("/author/:author", async (req, res) => {
    const data = await bookService.getByAuthor(req.params.author);
    res.json(data);
  });

  // (7) Get all books on particular shelf
  router.get("/shelf/:id", async (req, res) => {
    const data = await bookService.getShelf(req.params.id);
    res.json(data);
  });

  return router;
};

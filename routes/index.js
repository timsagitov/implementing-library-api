const express = require("express");
const booksRoute = require("./books");
const librarianRoute = require("./librarian");
const readerRoute = require("./reader");

const router = express.Router();

module.exports = (params) => {
  router.get("/", (req, res) => {
    res.send("Hello from /");
  });

  router.use("/books", booksRoute(params));
  router.use("/librarian", librarianRoute(params));
  router.use("/reader", readerRoute());

  return router;
};
